# -*- coding: utf-8 -*-
# Copyright (C) 2014 Sebastien Gedeon, IUT d'Orl�ans
#==============================================================================

from .action import Action3
from mud.events import ExplodeWithEvent

class ExplodeWithAction(Action3):
    EVENT = ExplodeWithEvent
    ACTION = "explode-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    